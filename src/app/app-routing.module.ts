import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  // {
  //   path: 'folder/:id',
  //   loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  // },

  // home
  {
    path: 'home', loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },

  // authentification
  {
    path: 'login', loadChildren: () => import('./pages/authentification/login/login.module').then( m => m.LoginPageModule)
  },
  // media
  {
    path: 'live', loadChildren: () => import('./pages/media/live/live.module').then( m => m.LivePageModule)
  },
  {
    path: 'picture-gallery', loadChildren: () => import('./pages/media/picture-gallery/picture-gallery.module').then( m => m.PictureGalleryPageModule)
  },
  {
    path: 'video-gallery', loadChildren: () => import('./pages/media/video-gallery/video-gallery.module').then( m => m.VideoGalleryPageModule)
  },
  // program
  {
    path: 'general-program', loadChildren: () => import('./pages/program/general-program/general-program.module').then( m => m.GeneralProgramPageModule)
  },
  {
    path: 'sector-program', loadChildren: () => import('./pages/program/sector-program/sector-program.module').then( m => m.SectorProgramPageModule)
  },
  // stat
  {
    path: 'general-result', loadChildren: () => import('./pages/stat/general-result/general-result.module').then( m => m.GeneralResultPageModule)
  },
  {
    path: 'detail-result', loadChildren: () => import('./pages/stat/detail-result/detail-result.module').then( m => m.DetailResultPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
