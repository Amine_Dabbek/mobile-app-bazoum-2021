import { Component, OnInit } from '@angular/core';

import { Platform, AlertController, ToastController, MenuController, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public appPagesMedia = [
    {
      title: 'Accueil',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Galerie photos',
      url: '/picture-gallery',
      icon: 'images'
    },
    {
      title: 'Bazoum Live',
      url: '/live',
      icon: 'videocam'
    },
    {
      title: 'Vidéo',
      url: '/video-gallery',
      icon: 'film'
    },
    {
      title: 'Programme',
      url: '/general-program',
      icon: 'document'
    }
  ];

  public pageElections = [
    {
      title: 'Resultats',
      url: '/general-result',
      icon: 'bar-chart'
    }];

  public PageSocial = [
    {
      title: 'Partager',
      url: '/partage',
      icon: 'share-social'
    },
    {
      title: 'Message',
      url: '/message',
      icon: 'send'
    }];

  // public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private menu: MenuController,
    public navCtrl: NavController,
    private alertCtrl: AlertController,
    private router: Router,
    public toastController: ToastController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
  }
  openpage(page) {
    this.router.navigateByUrl(page.url) 
  }
}
