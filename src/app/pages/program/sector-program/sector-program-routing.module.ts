import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SectorProgramPage } from './sector-program.page';

const routes: Routes = [
  {
    path: '',
    component: SectorProgramPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SectorProgramPageRoutingModule {}
