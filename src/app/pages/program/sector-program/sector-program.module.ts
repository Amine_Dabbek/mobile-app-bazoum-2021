import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SectorProgramPageRoutingModule } from './sector-program-routing.module';

import { SectorProgramPage } from './sector-program.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SectorProgramPageRoutingModule
  ],
  declarations: [SectorProgramPage]
})
export class SectorProgramPageModule {}
