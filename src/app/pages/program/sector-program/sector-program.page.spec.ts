import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SectorProgramPage } from './sector-program.page';

describe('SectorProgramPage', () => {
  let component: SectorProgramPage;
  let fixture: ComponentFixture<SectorProgramPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectorProgramPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SectorProgramPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
