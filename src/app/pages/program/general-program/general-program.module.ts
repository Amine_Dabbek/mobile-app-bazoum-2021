import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GeneralProgramPageRoutingModule } from './general-program-routing.module';

import { GeneralProgramPage } from './general-program.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GeneralProgramPageRoutingModule
  ],
  declarations: [GeneralProgramPage]
})
export class GeneralProgramPageModule {}
