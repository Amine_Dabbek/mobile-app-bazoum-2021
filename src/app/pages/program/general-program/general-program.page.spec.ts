import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GeneralProgramPage } from './general-program.page';

describe('GeneralProgramPage', () => {
  let component: GeneralProgramPage;
  let fixture: ComponentFixture<GeneralProgramPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralProgramPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GeneralProgramPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
