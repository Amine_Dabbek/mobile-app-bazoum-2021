import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GeneralProgramPage } from './general-program.page';

const routes: Routes = [
  {
    path: '',
    component: GeneralProgramPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GeneralProgramPageRoutingModule {}
