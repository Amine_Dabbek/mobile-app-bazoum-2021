import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PictureGalleryPage } from './picture-gallery.page';

const routes: Routes = [
  {
    path: '',
    component: PictureGalleryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PictureGalleryPageRoutingModule {}
