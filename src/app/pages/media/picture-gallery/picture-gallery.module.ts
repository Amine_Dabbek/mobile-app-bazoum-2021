import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PictureGalleryPageRoutingModule } from './picture-gallery-routing.module';

import { PictureGalleryPage } from './picture-gallery.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PictureGalleryPageRoutingModule
  ],
  declarations: [PictureGalleryPage]
})
export class PictureGalleryPageModule {}
