import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailResultPageRoutingModule } from './detail-result-routing.module';

import { DetailResultPage } from './detail-result.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailResultPageRoutingModule
  ],
  declarations: [DetailResultPage]
})
export class DetailResultPageModule {}
