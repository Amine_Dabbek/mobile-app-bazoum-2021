import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailResultPage } from './detail-result.page';

const routes: Routes = [
  {
    path: '',
    component: DetailResultPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailResultPageRoutingModule {}
