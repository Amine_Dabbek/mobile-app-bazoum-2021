import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailResultPage } from './detail-result.page';

describe('DetailResultPage', () => {
  let component: DetailResultPage;
  let fixture: ComponentFixture<DetailResultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailResultPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailResultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
