import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GeneralResultPage } from './general-result.page';

describe('GeneralResultPage', () => {
  let component: GeneralResultPage;
  let fixture: ComponentFixture<GeneralResultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralResultPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GeneralResultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
