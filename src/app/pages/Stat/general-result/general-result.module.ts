import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GeneralResultPageRoutingModule } from './general-result-routing.module';

import { GeneralResultPage } from './general-result.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GeneralResultPageRoutingModule
  ],
  declarations: [GeneralResultPage]
})
export class GeneralResultPageModule {}
