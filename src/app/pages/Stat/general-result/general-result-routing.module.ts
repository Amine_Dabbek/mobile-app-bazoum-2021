import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GeneralResultPage } from './general-result.page';

const routes: Routes = [
  {
    path: '',
    component: GeneralResultPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GeneralResultPageRoutingModule {}
