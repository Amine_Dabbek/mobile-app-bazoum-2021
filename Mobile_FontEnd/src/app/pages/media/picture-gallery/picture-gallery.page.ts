import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MediaService } from '../../../services/pages/media.service';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';


@Component({
  selector: 'app-picture-gallery',
  templateUrl: './picture-gallery.page.html',
  styleUrls: ['./picture-gallery.page.scss'],
})
export class PictureGalleryPage implements OnInit {

  images: [];
  urlImage = 'http://localhost:3030/';

  constructor(public navCtrl: NavController,
    private httpClient: HttpClient,
    private router: Router,
    private mediaAPI: MediaService,
    private photoViewer: PhotoViewer) { }

  ngOnInit() {
      this.getimages();
    // this.photoViewer.show("http://localhost:3030/uploads/images/0cae7affb734cbfb45a40ce649ce5310.jpg","jj",{share : true});
  }

  public getimages() {
    this.mediaAPI.getimages().subscribe(response => {
      console.log(response.status);
      this.images = response.data;
      response.data.forEach(element => {
        console.log(element);
      });
    }, (err) => {
      console.log(err);
    });
  }
}