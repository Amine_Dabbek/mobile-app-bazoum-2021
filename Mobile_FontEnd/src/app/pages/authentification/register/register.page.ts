import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { NavController, AlertController, LoadingController, ToastController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControlName, Validator, FormControl, Validators, AbstractControl } from '@angular/forms';


const registerUrl = 'https://erp.hbyomar.com/mobileapp/api/Authentication/Registeeeer_';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  public FirstName: string;
  public LastName: string;
  public Email: string;
  public Pays: string;
  public PhoneNumber: number;
  public loadingAlert: any;
  public registerForm: FormGroup;
  public result: any = [];
  public Success: any;
  public Errors = {
    'FirstName': [{
      type: 'required',
      message: 'Veuillez renseigner votre prénom.'
    },
    {
      type: 'minlength',
      message: 'Votre prénom doit comporter entre 2 et 30 caractères.'
    },
    {
      type: 'maxlength',
      message: 'Votre prénom doit comporter un maximum de 30 caractères .'
    },
    ],
    'LastName': [{
      type: 'required',
      message: 'Veuillez renseigner votre nom.'
    },
    {
      type: 'minlength',
      message: 'Votre nom doit comporter entre 2 et 30 caractères.'
    },
    {
      type: 'maxlength',
      message: 'Votre nom doit comporter un maximum de 30 caractères .'
    },
    ],
    'Pays': [{
      type: 'required',
      message: 'Veuillez renseigner votre pays.'
    },
    ],
    'Email': [{
      type: 'required',
      message: 'Veuillez renseigner votre Email.'
    },
    {
      type: 'minlength',
      message: 'Votre Email doit comporter au moins 5 caractères.'
    },
    {
      type: 'pattern',
      message: 'Adresse mail non valide.'
    },
    ],
    'PhoneNumber': [{
      type: 'required',
      message: 'Veuillez renseigner votre numéro de téléphone.'
    },
    {
      type: 'pattern',
      message: 'Votre numéro de téléphone doit comporter 8 chiffre.'
    }
    ]
  };

  constructor(public navCtrl: NavController,
    public httpClient: HttpClient,
    public alertCtrl: AlertController,
    private router: Router,
    private loadingCtrl: LoadingController,
    private toast: ToastController,
    public formBuilder: FormBuilder) {

    this.registerForm = this.formBuilder.group({
      PhoneNumber: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(5),
      ])),
      FirstName: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30),
      ])),
      LastName: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30),
      ])),
      Pays: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30),
      ])),
      Email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")
      ])),
    },
    );
  }

  ngOnInit() {
    this.createLoadingAlert();
  }

  async showAlert(title: string, content: string) {
    const alert = await this.alertCtrl.create({
      header: title,
      message: content,
      buttons: ['Ok']
    });
    await alert.present();
  }
  async createLoadingAlert() {
    this.loadingAlert = await this.loadingCtrl.create({
      message: 'Veuillez patienter svp...',
      spinner: 'crescent',
    });
  }
  async showLoadingAlert() {
    await this.loadingAlert.present();
  }
  async closeLoadingAlert() {
    await this.loadingAlert.dismiss();
  }
  async presentToast() {
    const toastController = await this.toast.create({
      message: 'Your settings have been saved.',
      duration: 2000
    });
    toastController.present();
  }

  public SingupUser() {

    this.showLoadingAlert();


    const requestData = {
      FirstName: this.FirstName,
      LastName: this.LastName,
      Pays: this.Pays,
      PhoneNumber: this.PhoneNumber,
      Email: this.Email,
    };

    this.httpClient.post(registerUrl, requestData).subscribe(response => {
      this.result = response;
      if (!this.result.Success) {
        this.showAlert('Attention', this.result.ErrorsLinesCombined);
      } else {
        this.router.navigateByUrl('account-validation');
      }
      this.closeLoadingAlert();
    }, error => {
      this.closeLoadingAlert();
    }
    );
  }
}
