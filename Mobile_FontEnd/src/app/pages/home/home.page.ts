import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MediaService } from '../../services/pages/media.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  imagesSlideOptions = {
    effect: 'flip',
    loop: true,
    autoplay: {
      delay: 2500,
    },
  };
  images: [];
  urlImage = 'http://localhost:3030/';

  // public path: string[];
  constructor(public navCtrl: NavController,
    private httpClient: HttpClient,
    private router: Router,
    private mediaAPI: MediaService,
    private socialSharing: SocialSharing
    // public navExtras: NavExtrasService,
    // public Deviceservice: DeviceHelperService,
    // public AuthService: AuthenticationService,
    // private alertCtrl: AlertController,
    // private platform: Platform,
    // private appComponent: AppComponent,
    // private callNumber: CallNumber,
  ) { }

  ngOnInit() {
    this.getimages();
  }

  GoToLive() {
    this.router.navigateByUrl('/live');
  }

  public getimages() {
    this.mediaAPI.getimages().subscribe(response => {
      console.log(response.status);
      this.images = response.data;
      response.data.forEach(element => {
        //console.log(element);
      });
    }, (err) => {
      console.log(err);
    });
  }

  public ShareApp() {
    this.socialSharing.share('Application Mobile Bazoum 2021\n' + 'https://play.google.com/store/apps/details?id=app.Bazoum2021.com');
  }

  GoToAdherer() {
    this.router.navigateByUrl('/register');
  }

  GoToStat() {
    this.router.navigateByUrl('/general-result');
  }

  GoToProgram() {
    this.router.navigateByUrl('/general-program');
  }

}

// export class data {
//   _id: string;
//   name: string;
//   title: string;
//   path: string;
// }

// export class images {
//   status: string;
//   message: string;
//   data: [] = [];
// }