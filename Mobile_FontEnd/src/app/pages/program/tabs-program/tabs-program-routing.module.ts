import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TabsProgramPage } from './tabs-program.page';

const routes: Routes = [
  {
    path: 'tabs-program',
    component: TabsProgramPage,
    children:
      [
        {
          path: 'general-program',
          children:
            [
              {
                path: '',
                loadChildren: () => import('../general-program/general-program.module').then(m => m.GeneralProgramPageModule)
              }
            ]
        },
        {
          path: 'sector-program',
          children:
            [
              {
                path: '',
                loadChildren: () => import('../sector-program/sector-program.module').then(m => m.SectorProgramPageModule)

              }
            ]
        },
        {
          path: '',
          redirectTo: '',
          pathMatch: 'full'
        }
      ]
  },
  {
    path: '',
    redirectTo: 'tabs-program/general-program',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsProgramPageRoutingModule { }
