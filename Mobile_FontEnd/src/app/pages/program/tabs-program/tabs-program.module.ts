import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TabsProgramPageRoutingModule } from './tabs-program-routing.module';

import { TabsProgramPage } from './tabs-program.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TabsProgramPageRoutingModule
  ],
  declarations: [TabsProgramPage]
})
export class TabsProgramPageModule {}
