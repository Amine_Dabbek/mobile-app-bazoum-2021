import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TabsProgramPage } from './tabs-program.page';

describe('TabsProgramPage', () => {
  let component: TabsProgramPage;
  let fixture: ComponentFixture<TabsProgramPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabsProgramPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TabsProgramPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
