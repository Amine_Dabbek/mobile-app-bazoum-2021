import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class MediaService {
   image;

  constructor(private httpClient: HttpClient) { }

  // public getimagesApi() {
  //   alert(1);
  //   this.httpClient.get('http://localhost:3030/api/images').subscribe(response => {
  //     this.image= response.data;
  //     this.image.forEach(element => {
  //       console.log(element.name);
  //       console.log(element.path);
  //       console.log(element.title);
  //     });
  //   }, (err) => {
  //     console.log(err);
  //   });
  // }


   getimages() {
     return this.httpClient.get<images>('http://localhost:3030/api/image')
  }
}


export class data {
  _id: string;
  name: string;
  title: string;
  path: string;
}

export class images {
  status: string;
  message: string;
  data: [] = [];
}